<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=bitmphp52;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `course` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $course = $row;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-4">
            <nav>
                <li><a href="index.php">All Course</a></li>
            </nav>
            <form action="update.php" method="post">
                <fieldset>
                    <legend>Course Infomation</legend>

                    <input type="hidden" class="form-control" id="id" name="id" placeholder="Enter First Name"
                           value="<?=$course['id'];?>">

                    <div class="form-group">
                        <label for="subject_code">Subject Code:</label>
                        <input type="text" class="form-control" id="subject_code" name="subject_code" placeholder="Enter Subject Code"
                               value="<?=$course['subject_code'];?>">
                    </div>
                    <div class="form-group">
                        <label for="subject_title">Subject Title:</label>
                        <input type="text" class="form-control" id="subject_title" name="subject_title" placeholder="Enter Subject Title"
                               value="<?=$course['subject_title'];?>">
                    </div>
                    <div class="form-group">
                        <label for="department">Department :</label>
                        <input value="<?=$course['department'];?>" type="text" class="form-control" id="department" name="department" placeholder="Enter Department">
                    </div>
                    <div class="form-group">
                        <label for="shift">Shift</label>
                        <input value="<?=$course['shift'];?>" type="text" class="form-control" id="shift" name="shift" placeholder="Enter your Shift">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </fieldset>
            </form>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>