<?php

// connection to db
$db = new PDO('mysql:host=localhost;dbname=bitmphp52;charset=utf8mb4', 'root', '');

//build query
$query = "SELECT * FROM `course` WHERE id = ".$_GET['id'];

//execute the query using php
foreach ($db->query($query) as $row){
    $course = $row;
}


?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div><a href="create.html"> Add New </a></div>
            <dl>
                <dt>id</dt>
                <dd><?php echo $course['id'];?></dd>

                <dt>Subject Code </dt>
                <dd><?php echo $course['subject_code'];?></dd>

                <dt>Subject Title </dt>
                <dd><?php echo $course['subject_title'];?></dd>

                <dt>Department</dt>
                <dd><?php echo $course['department'];?>

                <dt>Department</dt>
                <dd><?php echo $course['shift'];?></dd>

                <dt>Created</dt>
                <dd><?php echo date("d/m/Y", strtotime($course['create_at']));?></dd>

                <dt>Modified</dt>
                <dd><?php echo date("d/m/Y", strtotime($course['modified_at']));?></dd>
            </dl>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>