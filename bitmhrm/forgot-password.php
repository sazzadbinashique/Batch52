<!DOCTYPE html>
<html class="no-mobile no-mac" lang="en">
<head>
    <meta charset="UTF-8">
    <title>Advanced HRM - Forget Password</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/font-awesome/css/font-awesome.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/alertify/css/alertify.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/alertify/css/alertify-bootstrap-3.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/css/style.css">


</head>
<body>

<main id="wrapper" class="wrapper">
    <div class="container jumbo-container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="app-logo-inner text-center">
                    <img src="assets/img/logo.png" alt="logo" class="bar-logo">
                </div>

					<div class="panel panel-30">
						<div class="panel-heading">
							<h3 class="panel-title text-center">Reset your password</h3>
						</div>
						<div class="panel-body">
							<form class="" role="form" action="" method="post">

								<div class="form-group form-group-default required">
									<label for="el1">Email</label>
									<input type="email" name="email" class="form-control" required="">
								</div>

                                <input type="hidden" name="_token" value="">
								<button type="submit" class="btn btn-primary btn-block btn-lg">Reset Your Password</button>
							</form>
							<br>
													</div>
					</div>
					<div class="panel-other-acction">
						<div class="text-sm text-center">
							<a href="index.php">Back To Sign in</a>
						</div>
					</div>
            </div>
        </div>
    </div>
</main>

<script src="assets/libs/jquery-1.10.2.min.js"></script>

<script src="assets/libs/jquery.slimscroll.min.js"></script>

<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<script src="assets/libs/alertify/js/alertify.js"></script>

<script src="assets/js/scripts.js"></script>



</body>

</html>
