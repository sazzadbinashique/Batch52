<?php
require_once 'header.php';
?>




<section class="wrapper-bottom-sec">
    <div class="p-30">
        <h2 class="page-title">Attendance</h2>
    </div>
    <div class="p-30 p-t-none p-b-none">

        <div class="row">

            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Attendance</h3>
                    </div>
                    <div class="panel-body p-none">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0" type="search"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table data-table table-hover dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        <thead>
                                            <tr role="row"><th style="width: 133px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending">Date</th><th style="width: 133px;" class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Clock In: activate to sort column descending" aria-sort="ascending">Clock In</th><th style="width: 133px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Clock Out: activate to sort column ascending">Clock Out</th><th style="width: 72px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Late: activate to sort column ascending">Late</th><th style="width: 72px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Early Leaving: activate to sort column ascending">Early Leaving</th><th style="width: 72px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Overtime: activate to sort column ascending">Overtime</th><th style="width: 72px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Total Work: activate to sort column ascending">Total Work</th><th style="width: 133px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th></tr>
                                        </thead>
                                        <tbody>

                                            <tr role="row" class="odd">
                                                <td data-label="Date" class="">10th May 17</td>
                                                <td data-label="Clock_In" class="sorting_1">10:00 AM</td>
                                                <td data-label="Clock_out">7:00 PM</td>
                                                <td data-label="Late">0.58 H</td>
                                                <td data-label="Early_leaving">0 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">7.42 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="even">
                                                <td data-label="Date" class="">11th May 17</td>
                                                <td data-label="Clock_In" class="sorting_1">11:57 PM</td>
                                                <td data-label="Clock_out">11:57 PM</td>
                                                <td data-label="Late">14.53 H</td>
                                                <td data-label="Early_leaving">0 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">-6.53 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="odd">
                                                <td data-label="Date" class="">30th May 17</td>
                                                <td data-label="Clock_In" class="sorting_1">12:00 AM</td>
                                                <td data-label="Clock_out">12:00 AM</td>
                                                <td data-label="Late">0 H</td>
                                                <td data-label="Early_leaving">17.42 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">-9.42 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="even">
                                                <td data-label="Date" class="">9th Jun 17</td>
                                                <td data-label="Clock_In" class="sorting_1">12:00 AM</td>
                                                <td data-label="Clock_out">12:00 AM</td>
                                                <td data-label="Late">0 H</td>
                                                <td data-label="Early_leaving">17.42 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">-9.42 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="odd">
                                                <td data-label="Date" class="">9th May 17</td>
                                                <td data-label="Clock_In" class="sorting_1">12:00 AM</td>
                                                <td data-label="Clock_out">2:00 PM</td>
                                                <td data-label="Late">0 H</td>
                                                <td data-label="Early_leaving">3.42 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">4.58 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="even">
                                                <td data-label="Date" class="">1st Mar 17</td>
                                                <td data-label="Clock_In" class="sorting_1">9:00 AM</td>
                                                <td data-label="Clock_out">5:25 PM</td>
                                                <td data-label="Late">0 H</td>
                                                <td data-label="Early_leaving">0 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">8 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="odd">
                                                <td data-label="Date" class="">2nd Mar 17</td>
                                                <td data-label="Clock_In" class="sorting_1">9:20 AM</td>
                                                <td data-label="Clock_out">5:25 PM</td>
                                                <td data-label="Late">0 H</td>
                                                <td data-label="Early_leaving">0 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">8 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="even">
                                                <td data-label="Date" class="">5th Mar 17</td>
                                                <td data-label="Clock_In" class="sorting_1">9:20 AM</td>
                                                <td data-label="Clock_out">4:25 PM</td>
                                                <td data-label="Late">0 H</td>
                                                <td data-label="Early_leaving">1 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">7 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr><tr role="row" class="odd">
                                                <td data-label="Date" class="">4th Mar 17</td>
                                                <td data-label="Clock_In" class="sorting_1">9:55 AM</td>
                                                <td data-label="Clock_out">5:30 PM</td>
                                                <td data-label="Late">0.5 H</td>
                                                <td data-label="Early_leaving">0 H</td>
                                                <td data-label="Overtime">0 H</td>
                                                <td data-label="Total_Work">7.5 H</td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Present</p></td>
                                            </tr></tbody>
                                    </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 9 of 9 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button next disabled" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Next</a></li></ul></div></div></div></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>


<input id="_url" value="http://coderpixel.com/demo/advanced-hrm" type="hidden">
</main>
<?php
require_once 'footer.php';
?>