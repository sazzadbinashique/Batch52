<!DOCTYPE html>
<html class="no-mobile no-mac" lang="en">

<head>
    <meta charset="UTF-8">
    <title>BITM HRM - Signup</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/font-awesome/css/font-awesome.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/alertify/css/alertify.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/alertify/css/alertify-bootstrap-3.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/css/style.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/css/admin.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/css/responsive.css">


</head>
<body>

<main id="wrapper" class="wrapper">

<!--    <a href="apply-job.php" class="btn btn-success pull-right m-30">Apply Job</a>-->
    <div class="container jumbo-container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="app-logo-inner text-center">
                    <img src="assets/img/logo.png" alt="logo" class="bar-logo">
                </div>
                <div class="panel panel-30">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">SignUp to your account</h3>
                    </div>
                    <div class="panel-body">
                        <form class="" role="form" method="post" action="dashboard.php">

                            <div class="form-group form-group-default required">
                                <label for="first_name">First Name</label>
                                <input type="text" id="el1" class="form-control" required name="first_name">
                            </div>
                            <div class="form-group form-group-default required">
                                <label for="last_name">Last Name</label>
                                <input type="text" id="el1" class="form-control" required name="last_name">
                            </div>
                            <div class="form-group form-group-default required">
                                <label for="username">Last Name</label>
                                <input type="text" id="el1" class="form-control" required name="username">
                            </div>
                            <div class="form-group form-group-default required">
                                <label for="email">Email Name</label>
                                <input type="email" id="el1" class="form-control" required name="email">
                            </div>
                            <div class="form-group form-group-default required">
                                <label for="password">Password</label>
                                <input type="password" id="password" class="form-control" required name="password">
                            </div>

                            <div class="form-group m-t-20 m-b-20">
                                <div class="coder-checkbox">
                                    <input type="checkbox" checked name="remember">
                                    <span class="co-check-ui"></span>
                                    <label>Remember Me</label>
                                </div>
                            </div>
                            <input type="hidden" name="_token" value="">
                            <input type="submit" class="btn btn-primary btn-block btn-lg" value="SignUp">

                        </form>
                        <br>
                        <div class="panel-other-acction">
                            <div class="text-sm text-center right">
                                <a href="index.php">Back To Sign in</a> | <a href="forgot-password.php">Forget Password?</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</main>

<script src="assets/libs/jquery-1.10.2.min.js"></script>

<script src="assets/libs/jquery.slimscroll.min.js"></script>

<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<script src="assets/libs/alertify/js/alertify.js"></script>

<script src="assets/js/scripts.js"></script>

</body>

</html>
