<?php
require_once 'header.php';
?>
    
    
    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Leave Application</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">
                        <div class="row">

                <div class="col-lg-3">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">All Leave Details</h3>
                        </div>
                        <div class="panel-body p-none">
                            <table class="table table-hover">
                                                                <tbody><tr>
                                    <td>Sick Leave</td>
                                    <td>12</td>
                                </tr>
                                                                <tr>
                                    <td>Causal Leave</td>
                                    <td>12</td>
                                </tr>
                                                                <tr>
                                    <td>Total Leave</td>
                                    <td>24</td>
                                </tr>
                            </tbody></table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Leave Application</h3>
                            <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#new-leave"><i class="fa fa-plus"></i> New Leave
                            </button>
                            <br>
                        </div>
                        <div class="panel-body p-none">
                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0" type="search"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table data-table table-hover dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                <thead>
                                <tr role="row"><th style="width: 23px;" class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="SL#: activate to sort column descending">SL#</th><th style="width: 125px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Leave Type: activate to sort column ascending">Leave Type</th><th style="width: 83px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Leave From: activate to sort column ascending">Leave From</th><th style="width: 83px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Leave To: activate to sort column ascending">Leave To</th><th style="width: 84px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th><th style="width: 210px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Remark: activate to sort column ascending">Remark</th></tr>
                                </thead>
                                <tbody>
                                                                    

                                                                    

                                                                    

                                
                                <tr role="row" class="odd">
                                        <td data-label="SL" class="sorting_1">22</td>
                                        <td data-label="leaveType"><p>Sick Leave</p></td>
                                        <td data-label="LeaveFrom"><p>1st Apr 17</p></td>
                                        <td data-label="LeaveTo"><p>3rd Apr 17</p></td>
                                                                                    <td data-label="Status"><p class="btn btn-danger btn-xs">Rejected</p></td>
                                        
                                        <td data-label="remark">
                                            Get Well soon!!!
                                        </td>
                                    </tr><tr role="row" class="even">
                                        <td data-label="SL" class="sorting_1">23</td>
                                        <td data-label="leaveType"><p>Causal Leave</p></td>
                                        <td data-label="LeaveFrom"><p>4th Apr 17</p></td>
                                        <td data-label="LeaveTo"><p>6th Apr 17</p></td>
                                                                                    <td data-label="Status"><p class="btn btn-danger btn-xs">Rejected</p></td>
                                        
                                        <td data-label="remark">
                                            Already taken!!
                                        </td>
                                    </tr><tr role="row" class="odd">
                                        <td data-label="SL" class="sorting_1">24</td>
                                        <td data-label="leaveType"><p>Causal Leave</p></td>
                                        <td data-label="LeaveFrom"><p>10th May 17</p></td>
                                        <td data-label="LeaveTo"><p>10th Jun 17</p></td>
                                                                                    <td data-label="Status"><p class="btn btn-success btn-xs">Approved</p></td>
                                        
                                        <td data-label="remark">
                                            
                                        </td>
                                    </tr></tbody>
                            </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button next disabled" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Next</a></li></ul></div></div></div></div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Modal -->
            <div class="modal fade" id="new-leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Request For New Leave</h4>
                        </div>
                        <form class="form-some-up" role="form" method="post" action="">

                            <div class="modal-body">

                                <div class="form-group">
                                    <label>Leave Type</label>
                                    <select name="leave_type" id="e20" class="form-control selectpicker bs-select-hidden" data-live-search="true">
                                                                                    <option value="1">Sick Leave</option>
                                                                                    <option value="2">Causal Leave</option>
                                                                            </select><div class="btn-group bootstrap-select form-control"><button type="button" class="btn dropdown-toggle btn-default" data-toggle="dropdown" data-id="e20" title="Sick Leave"><span class="filter-option pull-left">Sick Leave</span>&nbsp;<span class="caret"></span></button><div class="dropdown-menu open"><div class="bs-searchbox"><input class="form-control" autocomplete="off" type="text"></div><ul class="dropdown-menu inner" role="menu"><li data-original-index="0" class="selected"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Sick Leave</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li><li data-original-index="1"><a tabindex="0" class="" style="" data-tokens="null"><span class="text">Causal Leave</span><span class="glyphicon glyphicon-ok check-mark"></span></a></li></ul></div></div>
                                </div>


                                <div class="form-group">
                                    <label>Leave From</label>
                                    <input class="form-control datePicker" name="leave_from" required="" type="text">
                                </div>

                                <div class="form-group">
                                    <label>Leave To</label>
                                    <input class="form-control datePicker" name="leave_to" required="" type="text">
                                </div>


                                <div class="form-group">
                                    <label>Leave Reason</label>
                                    <ul class="wysihtml5-toolbar" style=""><li class="dropdown">
  <a class="btn btn-default dropdown-toggle " data-toggle="dropdown">
    
      <span class="fa fa-font"></span>
    
    <span class="current-font">Normal text</span>
    <b class="caret"></b>
  </a>
  <ul class="dropdown-menu">
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="p" tabindex="-1" href="javascript:;" unselectable="on">Normal text</a></li>
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h1" tabindex="-1" href="javascript:;" unselectable="on">Heading 1</a></li>
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h2" tabindex="-1" href="javascript:;" unselectable="on">Heading 2</a></li>
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h3" tabindex="-1" href="javascript:;" unselectable="on">Heading 3</a></li>
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h4" tabindex="-1" href="javascript:;" unselectable="on">Heading 4</a></li>
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h5" tabindex="-1" href="javascript:;" unselectable="on">Heading 5</a></li>
    <li><a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="h6" tabindex="-1" href="javascript:;" unselectable="on">Heading 6</a></li>
  </ul>
</li>
<li>
  <div class="btn-group">
    <a class="btn  btn-default" data-wysihtml5-command="bold" title="CTRL+B" tabindex="-1" href="javascript:;" unselectable="on">Bold</a>
    <a class="btn  btn-default" data-wysihtml5-command="italic" title="CTRL+I" tabindex="-1" href="javascript:;" unselectable="on">Italic</a>
    <a class="btn  btn-default" data-wysihtml5-command="underline" title="CTRL+U" tabindex="-1" href="javascript:;" unselectable="on">Underline</a>
    
    <a class="btn  btn-default" data-wysihtml5-command="small" title="CTRL+S" tabindex="-1" href="javascript:;" unselectable="on">Small</a>
    
  </div>
</li>
<li>
  <a class="btn  btn-default" data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote" data-wysihtml5-display-format-name="false" tabindex="-1" href="javascript:;" unselectable="on">
     
      <span class="fa fa-quote-left"></span>
    
  </a>
</li>
<li>
  <div class="btn-group">
    <a class="btn  btn-default" data-wysihtml5-command="insertUnorderedList" title="Unordered list" tabindex="-1" href="javascript:;" unselectable="on">
    
      <span class="fa fa-list-ul"></span>
    
    </a>
    <a class="btn  btn-default" data-wysihtml5-command="insertOrderedList" title="Ordered list" tabindex="-1" href="javascript:;" unselectable="on">
    
      <span class="fa fa-list-ol"></span>
    
    </a>
    <a class="btn  btn-default" data-wysihtml5-command="Outdent" title="Outdent" tabindex="-1" href="javascript:;" unselectable="on">
    
      <span class="fa fa-outdent"></span>
    
    </a>
    <a class="btn  btn-default" data-wysihtml5-command="Indent" title="Indent" tabindex="-1" href="javascript:;" unselectable="on">
    
      <span class="fa fa-indent"></span>
    
    </a>
  </div>
</li>
<li>
  <div class="bootstrap-wysihtml5-insert-link-modal modal fade" data-wysihtml5-dialog="createLink">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <a class="close" data-dismiss="modal">×</a>
          <h3>Insert link</h3>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input value="http://" class="bootstrap-wysihtml5-insert-link-url form-control" data-wysihtml5-dialog-field="href">
          </div> 
          <div class="checkbox">
            <label> 
              <input class="bootstrap-wysihtml5-insert-link-target" checked="" type="checkbox">Open link in new window
            </label>
          </div>
        </div>
        <div class="modal-footer">
          <a class="btn btn-default" data-dismiss="modal" data-wysihtml5-dialog-action="cancel" href="#">Cancel</a>
          <a href="#" class="btn btn-primary" data-dismiss="modal" data-wysihtml5-dialog-action="save">Insert link</a>
        </div>
      </div>
    </div>
  </div>
  <a class="btn  btn-default" data-wysihtml5-command="createLink" title="Insert link" tabindex="-1" href="javascript:;" unselectable="on">
    
      <span class="fa fa-share-square-o"></span>
    
  </a>
</li>
<li>
  <div class="bootstrap-wysihtml5-insert-image-modal modal fade" data-wysihtml5-dialog="insertImage">
    <div class="modal-dialog ">
      <div class="modal-content">
        <div class="modal-header">
          <a class="close" data-dismiss="modal">×</a>
          <h3>Insert image</h3>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input value="http://" class="bootstrap-wysihtml5-insert-image-url form-control" data-wysihtml5-dialog-field="src">
          </div> 
        </div>
        <div class="modal-footer">
          <a class="btn btn-default" data-dismiss="modal" data-wysihtml5-dialog-action="cancel" href="#">Cancel</a>
          <a class="btn btn-primary" data-dismiss="modal" data-wysihtml5-dialog-action="save" href="#">Insert image</a>
        </div>
      </div>
    </div>
  </div>
  <a class="btn  btn-default" data-wysihtml5-command="insertImage" title="Insert image" tabindex="-1" href="javascript:;" unselectable="on">
    
      <span class="fa fa-file-image-o"></span>
    
  </a>
</li>
</ul><textarea class="textarea-wysihtml5 form-control" name="leave_reason" style="display: none;"></textarea><input name="_wysihtml5_mode" value="1" type="hidden"><iframe class="wysihtml5-sandbox" security="restricted" allowtransparency="true" marginwidth="0" marginheight="0" style="display: block; background-color: rgb(255, 255, 255); border-collapse: separate; border-color: rgba(0, 0, 0, 0.07); border-style: solid; border-width: 1px; clear: none; float: none; margin: 0px; outline: 0px none rgb(68, 68, 51); outline-offset: 0px; padding: 6px 12px; position: static; top: auto; left: auto; right: auto; bottom: auto; z-index: auto; vertical-align: middle; text-align: start; box-sizing: border-box; box-shadow: none; border-radius: 4px; width: 100%; height: auto;" width="0" height="0" frameborder="0"></iframe>
                                </div>


                            </div>
                            <div class="modal-footer">
                                <input name="_token" value="" type="hidden">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
    </section>


    
    <input id="_url" value="" type="hidden">
</main>
<?php
require_once 'footer.php';
?>