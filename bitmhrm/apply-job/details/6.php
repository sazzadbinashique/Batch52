<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <title>Advanced HRM - Web Developer</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>
    <link media="all" type="text/css" rel="stylesheet" href="../../assets/libs/bootstrap/css/bootstrap.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="../../assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="../../assets/libs/font-awesome/css/font-awesome.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="../../assets/libs/alertify/css/alertify.css">

    <link media="all" type="text/css" rel="stylesheet" href="../../assets/libs/alertify/css/alertify-bootstrap-3.css">

    <link media="all" type="text/css" rel="stylesheet" href="../../assets/css/style.css">


</head>
<body class="has-top-bar">

<main id="wrapper" class="wrapper">

    <div class="top-bar">

        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"><i class="fa fa-bars"></i></button>
                <a class="navbar-brand" href="#">
                    <img src="../../assets/img/logo.png" alt="logo" class="bar-logo">
                </a>
            </div>

            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <ul class="nav navbar-nav navbar-right">
                    <li><a href="../../index.html">Home</a></li>
                    <li class="active"><a href="../../apply-job.html">Jobs</a></li>
                </ul>

            </div>
        </div>

    </div>

    <section class="wrapper-bottom-sec">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">

                    <h2 class="p-t-30 m-b-30 page-title">Web Developer</h2>

                    
                   
                    <div class="panel panel-30">
                        <div class="panel-body p-t-30">
                            <div class="row">

                                <div class="col-md-12">

                                    <div class="article-content">

                                        <div class="content-tight-panel">

                                            <div class="panel p-t-30">
                                                <div class="panel-body">
                                                    <h4 class="m-b-20">Job Summary</h4>
                                                    <ul class="info-list title-space-md">
                                                        <li>
                                                            <span class="info-list-title">Published on</span>
                                                            <span class="info-list-des">9th May 17</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Number Of Post</span>
                                                            <span class="info-list-des">4</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Job Type</span>
                                                            <span class="info-list-des">Part Time</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Experience</span>
                                                            <span class="info-list-des">123123</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Age</span>
                                                            <span class="info-list-des">123</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Job Location</span>
                                                            <span class="info-list-des">fddsf</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Salary Range</span>
                                                            <span class="info-list-des">sdfsdf</span>
                                                        </li>
                                                        <li>
                                                            <span class="info-list-title">Application Deadline</span>
                                                            <span class="info-list-des">30th Nov -1</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                        </div>

                                        <p>sdfsdf</p>


                                        <button class="btn btn-success" data-toggle="modal" data-target="#apply-now">Apply Now</button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    


                    <div class="modal fade" id="apply-now" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Apply For Web Developer</h4>
                                </div>
                                <form class="form-some-up" role="form" method="post" action="http://coderpixel.com/demo/advanced-hrm/apply-job/post-applicant-resume" enctype="multipart/form-data">

                                    <div class="modal-body">


                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" class="form-control" name="name" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="email" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input type="text" class="form-control" name="phone" required="">
                                        </div>

                                        <div class="form-group">
                                            <label>Upload Resume</label>
                                            <div class="input-group input-group-file">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-primary btn-file">
                                                        Browse <input type="file" class="form-control" name="resume" required="">
                                                    </span>
                                                </span>
                                                <input type="text" class="form-control" readonly="" >
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <input type="hidden" name="_token" value="BIuAt1CDey32fP3keUsqgcSDAKURbxH9BnN8DMJr">
                                        <input type="hidden" value="6" name="cmd">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Apply</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
    </section>
</main>

<script src="../../assets/libs/jquery-1.10.2.min.js"></script>

<script src="../../assets/libs/jquery.slimscroll.min.js"></script>

<script src="../../assets/libs/smoothscroll.min.js"></script>

<script src="../../assets/libs/bootstrap/js/bootstrap.min.js"></script>

<script src="../../assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<script src="../../assets/libs/alertify/js/alertify.js"></script>

<script src="../../assets/js/scripts.js"></script>



</body>

</html>
