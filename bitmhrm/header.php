<!DOCTYPE html>
<html class="no-mobile no-mac" lang="en">
    <meta charset="utf-8">
    <title>Advanced HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="http://fonts.googleapis.com/css?family=Roboto:400,300,500,700" rel="stylesheet" type="text/css">
    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap/css/bootstrap.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap-toggle/css/bootstrap-toggle.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/font-awesome/css/font-awesome.min.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/alertify/css/alertify.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/alertify/css/alertify-bootstrap-3.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/libs/bootstrap-select/css/bootstrap-select.min.css">



    <link media="all" type="text/css" rel="stylesheet" href="assets/css/style.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/css/admin.css">

    <link media="all" type="text/css" rel="stylesheet" href="assets/css/responsive.css">

    <!-- MORRIS CHART STYLES-->
    <link href="assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLES-->
    <link href="assets/css/custom.css" rel="stylesheet" />
    <!-- GOOGLE FONTS-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
</head>



<body class="has-left-bar has-top-bar">
    <nav id="left-nav" class="left-nav-bar">
        <div class="nav-top-sec">
            <div class="app-logo">
                <img src="assets/img/logo.png" alt="logo" class="bar-logo">
            </div>

            <a href="#" id="bar-setting" class="bar-setting"><i class="fa fa-bars"></i></a>
        </div>
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;">
            <div class="nav-bottom-sec" style="overflow: hidden; width: auto; height: 100%;">
                <ul class="left-navigation" id="left-navigation">

                    <li class="active"><a href="dashboard.php"><span class="menu-text">Dashboard</span> <span class="menu-thumb"><i class="fa fa-dashboard"></i></span></a></li>

                    <li class="has-sub  sub-open">
                        <a href="#"><span class="menu-text">Attendance</span> <span class="arrow"></span><span class="menu-thumb"><i class="fa fa-users"></i></span></a>
                        <ul class="sub" style="display: block;">
                            <li><a href="attendance.php"><span class="menu-text">Give Attendance</span> <span class="menu-thumb"><i class="fa fa-users"></i></span></a></li>

                            <li><a href="attendance_report.php"><span class="menu-text">Attendance Report</span> <span class="menu-thumb"><i class="fa fa-user-plus"></i></span></a></li>
                        </ul>
                    </li>

                    <li class="has-sub  sub-open">
                        <a href="#"><span class="menu-text">Employee</span> <span class="arrow"></span><span class="menu-thumb"><i class="fa fa-users"></i></span></a>
                        <ul class="sub" style="display: block;">
                            <li><a href="employee_add.php"><span class="menu-text">Add Employee</span> <span class="menu-thumb"><i class="fa fa-users"></i></span></a></li>
                            <li><a href="employee_edit.php"><span class="menu-text">Edit Employee Information</span> <span class="menu-thumb"><i class="fa fa-user-plus"></i></span></a></li>
                            <li><a href="employee_view.php"><span class="menu-text">All Employee List</span> <span class="menu-thumb"><i class="fa fa-user-plus"></i></span></a></li>
                        </ul>
                    </li>

                    <li class="has-sub  sub-open">
                        <a href="#"><span class="menu-text">Leave</span> <span class="arrow"></span><span class="menu-thumb"><i class="fa fa-users"></i></span></a>
                        <ul class="sub" style="display: block;">
                            <li><a href="leave_status.php"><span class="menu-text">Leave Status</span> <span class="menu-thumb"><i class="fa fa-users"></i></span></a></li>
                            <li><a href="leave_view.php"><span class="menu-text">Leave History</span> <span class="menu-thumb"><i class="fa fa-user-plus"></i></span></a></li>
                            <li><a href="leave_type.php"><span class="menu-text">Leave Type</span> <span class="menu-thumb"><i class="fa fa-user-plus"></i></span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main id="wrapper" class="wrapper">

        <div class="top-bar clearfix">


            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Attendanc<span class="caret"></span><span class="menu-thumb"></a>
                        <ul class="dropdown-menu">
                            <li><a href="attendance.php">Give Attendance</a></li>
                            <li><a href="attendance_report.php">Attendance Report</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Employee<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="employee_add.php">Add Employee</a></li>
                            <li><a href="employee_edit.php">Edit Employee Information</a></li>
                            <li><a href="employee_view.php">Employee List</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Leave<span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="leave_application.php">Leave Application</a></li>
                            <li><a href="leave_status.php">Leave Status</a></li>
                            <li><a href="leave_edit.php">Edit Leave</a></li>
                            <li><a href="leave_view.php">Leave History</a></li>
                        </ul>
                    </li>
                </ul>
                <form class="navbar-form navbar-left" action="https://www.google.com/">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
                <div class="navbar-right">
                    <div class="dropdown user-profile">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="user-info">Sazzad Bin Ashique</span>
                            <img class="user-image" src="assets/img/user.jpg" alt="user">

                        </a>
                        <ul class="dropdown-menu arrow right-arrow" role="menu">
                            <li><a href="user_edit.php"><i class="fa fa-edit"></i>Update Profile</a></li>
                            <li><a href="password_change.php"><i class="fa fa-lock"></i>Change Password</a></li>
                            <li class="bg-dark">
                                <a href="index.php" class="clearfix">
                                    <span class="pull-left">Logout</span>
                                    <span class="pull-right"><i class="fa fa-power-off"></i></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- /.navbar-collapse -->

        </div>