
    <script src="assets/libs/jquery-1.10.2.min.js"></script>

    <script src="assets/libs/jquery.slimscroll.min.js"></script>

    <script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>

    <script src="assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

    <script src="assets/libs/alertify/js/alertify.js"></script>

    <script src="assets/libs/bootstrap-select.min.js"></script>

    <script src="assets/js/scripts.js"></script>
    
    
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- MORRIS CHART SCRIPTS -->
     <script src="assets/js/morris/raphael-2.1.0.min.js"></script>
    <script src="assets/js/morris/morris.js"></script>
      <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
</body>
</html>