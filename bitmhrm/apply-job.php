
<?php
require_once 'header.php';
?>
<section class="wrapper-bottom-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <h2 class="p-t-30 m-b-30 page-title">All Jobs</h2>

                <div class="panel cp-clickable cp-linkable panel-hoverd panel-30">

                    <a href="apply-job/details/1.php" class="cp-area-link">Support Engineer</a>

                    <div class="panel-heading">
                        <h3 class="panel-title panel-title-15"><a href="apply-job/details/1.php">Support Engineer</a></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-5 col-md-12">
                                <ul class="info-list title-space-md">
                                    <li><span class="info-list-title">Number Of Post</span><span class="info-list-des">5</span></li>
                                    <li><span class="info-list-title">Experience</span><span class="info-list-des">1 to 2 year(s)</span></li>
                                    <li><span class="info-list-title">Deadline</span><span class="info-list-des">28th Feb 17</span></li>
                                </ul>
                            </div>


                            <div class="col-md-12 m-b-10 hidden-lg"></div>


                            <div class="col-lg-7 col-md-12">
                                <div class="last-child-m-b-n">
                                    <p>For the Support Division of CoderPixel are seeking application for the above position.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel cp-clickable cp-linkable panel-hoverd panel-30">


                    <a href="apply-job/details/5.php" class="cp-area-link">Gerente de Mercadeo</a>

                    <div class="panel-heading">
                        <h3 class="panel-title panel-title-15"><a href="apply-job/details/5.php">Gerente de Mercadeo</a></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-5 col-md-12">
                                <ul class="info-list title-space-md">
                                    <li><span class="info-list-title">Number Of Post</span><span class="info-list-des">10</span></li>
                                    <li><span class="info-list-title">Experience</span><span class="info-list-des">2 A&ntilde;os</span></li>
                                    <li><span class="info-list-title">Deadline</span><span class="info-list-des">1st Jun 17</span></li>
                                </ul>
                            </div>


                            <div class="col-md-12 m-b-10 hidden-lg"></div>


                            <div class="col-lg-7 col-md-12">
                                <div class="last-child-m-b-n">
                                    <p>Se necesita un gerente de mercadeo</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel cp-clickable cp-linkable panel-hoverd panel-30">


                    <a href="apply-job/details/6.php" class="cp-area-link">Web Developer</a>

                    <div class="panel-heading">
                        <h3 class="panel-title panel-title-15"><a href="apply-job/details/6.php">Web Developer</a></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-5 col-md-12">
                                <ul class="info-list title-space-md">
                                    <li><span class="info-list-title">Number Of Post</span><span class="info-list-des">4</span></li>
                                    <li><span class="info-list-title">Experience</span><span class="info-list-des">123123</span></li>
                                    <li><span class="info-list-title">Deadline</span><span class="info-list-des">30th Nov -1</span></li>
                                </ul>
                            </div>


                            <div class="col-md-12 m-b-10 hidden-lg"></div>


                            <div class="col-lg-7 col-md-12">
                                <div class="last-child-m-b-n">
                                    <p>szdfsd</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</section>

</main>

<script src="assets/libs/jquery-1.10.2.min.js"></script>

<script src="assets/libs/jquery.slimscroll.min.js"></script>

<script src="assets/libs/smoothscroll.min.js"></script>

<script src="assets/libs/bootstrap/js/bootstrap.min.js"></script>

<script src="assets/libs/bootstrap-toggle/js/bootstrap-toggle.min.js"></script>

<script src="assets/libs/alertify/js/alertify.js"></script>

<script src="assets/js/scripts.js"></script>



</body>

</html>
