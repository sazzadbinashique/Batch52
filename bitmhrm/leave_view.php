<?php

require_once 'header.php';
?>


<section class="wrapper-bottom-sec">

    <div class="p-30 p-t-none p-b-none">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Leave Details</h3>
                        <button class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#new-leave"><i class="fa fa-plus"></i> New Leave
                        </button>
                        <br>
                    </div>
                    <div class="panel-body p-none">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0" type="search"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table data-table table-hover dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        <thead>
                                            <tr role="row"><th style="width: 23px;" class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="SL#: activate to sort column descending">SL#</th><th style="width: 125px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Leave Type: activate to sort column ascending">Leave Type</th><th style="width: 83px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Leave From: activate to sort column ascending">Leave From</th><th style="width: 83px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Leave To: activate to sort column ascending">Leave To</th><th style="width: 84px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th><th style="width: 210px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Remark: activate to sort column ascending">Remark</th></tr>
                                        </thead>
                                        <tbody>
                                            <tr role="row" class="odd">
                                                <td data-label="SL" class="sorting_1">22</td>
                                                <td data-label="leaveType"><p>Sick Leave</p></td>
                                                <td data-label="LeaveFrom"><p>1st Apr 17</p></td>
                                                <td data-label="LeaveTo"><p>3rd Apr 17</p></td>
                                                <td data-label="Status"><p class="btn btn-danger btn-xs">Rejected</p></td>

                                                <td data-label="remark">
                                                    Get Well soon!!!
                                                </td>
                                            </tr><tr role="row" class="even">
                                                <td data-label="SL" class="sorting_1">23</td>
                                                <td data-label="leaveType"><p>Causal Leave</p></td>
                                                <td data-label="LeaveFrom"><p>4th Apr 17</p></td>
                                                <td data-label="LeaveTo"><p>6th Apr 17</p></td>
                                                <td data-label="Status"><p class="btn btn-danger btn-xs">Rejected</p></td>

                                                <td data-label="remark">
                                                    Already taken!!
                                                </td>
                                            </tr><tr role="row" class="odd">
                                                <td data-label="SL" class="sorting_1">24</td>
                                                <td data-label="leaveType"><p>Causal Leave</p></td>
                                                <td data-label="LeaveFrom"><p>10th May 17</p></td>
                                                <td data-label="LeaveTo"><p>10th Jun 17</p></td>
                                                <td data-label="Status"><p class="btn btn-success btn-xs">Approved</p></td>

                                                <td data-label="remark">

                                                </td>
                                            </tr></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries
                                    </div>

                                </div>
                                <div class="col-sm-7">
                                    <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                        <ul class="pagination">
                                            <li class="paginate_button previous disabled" id="DataTables_Table_0_previous">
                                                <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Previous</a>
                                            </li>
                                            <li class="paginate_button active">
                                                <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a>
                                            </li><li class="paginate_button next disabled" id="DataTables_Table_0_next">
                                                <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Next</a>
                                            </li></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>



<input id="_url" value="" type="hidden">
</main>
<?php

require_once 'footer.php';
?>