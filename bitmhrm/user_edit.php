<?php

require_once 'header.php';
?>

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">View Profile</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-body p-t-20">
                            <div class="clearfix">
                                <div class="pull-left m-r-30">
                                    <div class="thumbnail m-b-none">

                                        <img src="assets/img/user.jpg" alt="Profile Page" width="200px" height="200px">
                                    </div>
                                </div>
                                <div class="pull-left">
                                    <h3 class="bold font-color-1">Sazzad Bin Ashique</h3>
                                    <ul class="info-list">                                                                                    <li><span class="info-list-title">Email</span><span class="info-list-des">employee@coderpixel.com</span></li>


                                        <li><span class="info-list-title">Username</span><span class="info-list-des">employee</span></li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <div class="p-30 p-t-none p-b-none">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#personal_details" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Personal Details</a></li>

                        <li role="presentation" class=""><a href="#change-picture" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">Change Picture</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content panel p-20">



                        <div role="tabpanel" class="tab-pane active" id="personal_details">
                            <form role="form" method="post" action="#">

                                <div class="row">
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" required="" value="Abul Kashem" name="fname">
                                        </div>

                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" value="Shamim" name="lname">
                                        </div>


                                        <div class="form-group">
                                            <label>Email</label>
                                            <span class="help">e.g. "coderpixel@gmail.com" (Unique For every User)</span>
                                            <input type="email" class="form-control" required="" name="email" value="employee@coderpixel.com">
                                        </div>


<!--                                        <div class="form-group">-->
<!--                                            <label>Phone Number</label>-->
<!--                                            <input type="text" class="form-control" value="" name="phone">-->
<!--                                        </div>-->

                                    </div>

                                    <div class="col-md-12">
                                        <input type="hidden" name="_token" value="ZUdsKyIXMMAy0KYxHeXP7eIXI7ox1YDnp0WWJzX9">
                                        <input type="submit" value="Update" class="btn btn-success">

                                    </div>
                                </div>


                            </form>

                        </div>


                        <div role="tabpanel" class="tab-pane" id="change-picture">
                            <form role="form" action="#" method="post" enctype="multipart/form-data">

                                <div class="row">
                                    <div class="col-md-4">

                                        <div class="form-group input-group input-group-file">
                                                <span class="input-group-btn">
                                                    <span class="btn btn-primary btn-file">
                                                        Browse <input type="file" class="form-control" name="image">
                                                    </span>
                                                </span>
                                            <input type="text" class="form-control" readonly="">
                                        </div>

                                        <input type="hidden" name="_token" value="ZUdsKyIXMMAy0KYxHeXP7eIXI7ox1YDnp0WWJzX9">
                                        <input type="submit" value="Update" class="btn btn-primary">

                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php

require_once 'footer.php';
?>