<?php
require_once 'header.php';
?>

    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Employee Add Form</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="#" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> New Employee Add</h3>
<!--                                    <a href="#" class="btn btn-success">New Employee</a>-->
<!--                                    <a href="employee_view.php" class="btn btn-success">View Employee</a>-->
                                </div>

                                <div class="form-group">
                                    <label>First Name</label>
                                    <input class="form-control"  name="first_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input class="form-control"  name="Last_name" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control"  name="email" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Department</label>
                                    <input type="text" class="form-control"  name="department" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input class="form-control"  name="address" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Contact No</label>
                                    <input class="form-control"  name="contact" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Gender : </label>
                                    <input type="radio" name="gender" value="0">Female
                                    <input type="radio" name="gender"value="1">Male
                                </div>
                                <div class="form-group">
                                    <label>Age</label>
                                    <input type="number" class="form-control"  name="age" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Country</label>
                                    <select class="form-control"  name="" id="countryid">
                                        <option value="0">Bangladesh</option>
                                        <option value="0">India</option>
                                        <option value="0">Pakistan</option>
                                        <option value="0">United State</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>City</label>
                                    <select class="form-control"  id="" name="cityid" >

                                        <option value="0">Dhaka</option>
                                        <option value="0">Chittagoang</option>
                                        <option value="0">Rajshahi</option>
                                        <option value="0">Jessore</option>
                                        <option value="0">Narayanganj</option>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>National Id</label>
                                    <input class="form-control"  name="nid" value=""/>
                                </div>
                                <div class="form-group">
                                    <label>Gender : </label>
                                    <input type="radio" name="gender" value="0"> Married
                                    <input type="radio" name="gender"value="1"> Unmarried
                                    <input type="radio" name="gender"value="2"> Widow
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control"  name="address"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Photo</label>
                                    <input type="file" class="form-control"  name="photo">
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-primary" name="sub" value=" Save " />
                                </div>

<!--                                <input type="hidden" name="_token" value="ZUdsKyIXMMAy0KYxHeXP7eIXI7ox1YDnp0WWJzX9">-->
<!--                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Update </button>-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
require_once 'footer.php';
?>