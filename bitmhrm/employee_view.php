<?php

require_once 'header.php';
?>

<section class="wrapper-bottom-sec">
    <div class="p-30">
        <h2 class="page-title">Employees</h2>
    </div>
    <div class="p-30 p-t-none p-b-none">
        <div class="row">

            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Employees</h3>
                    </div>
                    <div class="panel-body p-none">
                        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer"><div class="row"><div class="col-sm-6"><div class="dataTables_length" id="DataTables_Table_0_length"><label>Show <select name="DataTables_Table_0_length" aria-controls="DataTables_Table_0" class="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-6"><div id="DataTables_Table_0_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control input-sm" placeholder="" aria-controls="DataTables_Table_0"></label></div></div></div><div class="row"><div class="col-sm-12"><table class="table data-table table-hover dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
                                        <thead>
                                        <tr role="row"><th style="width: 68px;" class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Code#: activate to sort column descending">Code#</th><th style="width: 186px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Name: activate to sort column ascending">Name</th><th style="width: 185px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Username: activate to sort column ascending">Username</th><th style="width: 185px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Designation: activate to sort column ascending">Designation</th><th style="width: 67px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending">Status</th><th style="width: 185px;" class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Actions: activate to sort column ascending">Actions</th></tr>
                                        </thead>
                                        <tbody>
                                        <tr role="row" class="odd">
                                            <td data-label="SL" class="sorting_1">5874698</td>
                                            <td data-label="Name"><p>Abul Kashem Shamim</p></td>
                                            <td data-label="Username"><p>employee</p></td>
                                            <td data-label="Designation"><p>Support Engineer</p></td>
                                            <td data-label="Status"><p class="btn btn-success btn-xs">Active</p></td>
                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="http://coderpixel.com/demo/advanced-hrm/employees/view/3"><i class="fa fa-edit"></i> Edit</a>
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="3"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr><tr role="row" class="even">
                                            <td data-label="SL" class="sorting_1">es100</td>
                                            <td data-label="Name"><p>Sanjay Dange</p></td>
                                            <td data-label="Username"><p>smartinfosysindia@gmail.com</p></td>
                                            <td data-label="Designation"><p>Support Engineer</p></td>
                                            <td data-label="Status"><p class="btn btn-success btn-xs">Active</p></td>
                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="http://coderpixel.com/demo/advanced-hrm/employees/view/24"><i class="fa fa-edit"></i> Edit</a>
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="24"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr><tr role="row" class="odd">
                                            <td data-label="SL" class="sorting_1">GT009</td>
                                            <td data-label="Name"><p>Abubakar </p></td>
                                            <td data-label="Username"><p>1</p></td>
                                            <td data-label="Designation"><p>Web Developer</p></td>
                                            <td data-label="Status"><p class="btn btn-success btn-xs">Active</p></td>
                                            <td data-label="Actions">
                                                <a class="btn btn-success btn-xs" href="http://coderpixel.com/demo/advanced-hrm/employees/view/23"><i class="fa fa-edit"></i> Edit</a>
                                                <a href="#" class="btn btn-danger btn-xs cdelete" id="23"><i class="fa fa-trash"></i> Delete</a>
                                            </td>
                                        </tr></tbody>
                                    </table></div></div><div class="row"><div class="col-sm-5"><div class="dataTables_info" id="DataTables_Table_0_info" role="status" aria-live="polite">Showing 1 to 3 of 3 entries</div></div><div class="col-sm-7"><div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate"><ul class="pagination"><li class="paginate_button previous disabled" id="DataTables_Table_0_previous"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="0" tabindex="0">Previous</a></li><li class="paginate_button active"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a></li><li class="paginate_button next disabled" id="DataTables_Table_0_next"><a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">Next</a></li></ul></div></div></div></div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>

<?php

require_once 'footer.php';
?>