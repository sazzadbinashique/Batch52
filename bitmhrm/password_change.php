<?php

require_once 'header.php';
?>


    <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">Change Password</h2>
        </div>
        <div class="p-30 p-t-none p-b-none">

            <div class="row">

                <div class="col-lg-6">
                    <div class="panel">
                        <div class="panel-body">
                            <form class="" role="form" action="http://coderpixel.com/demo/advanced-hrm/employee/update-employee-password" method="post">
                                <div class="panel-heading">
                                    <h3 class="panel-title"> Change Password</h3>
                                </div>

                                <div class="form-group">
                                    <label>Current Password</label>
                                    <input type="password" class="form-control" required="" name="current_password">
                                </div>

                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control" required="" name="new_password">
                                </div>

                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" required="" name="confirm_password">
                                </div>

                                <input type="hidden" name="_token" value="ZUdsKyIXMMAy0KYxHeXP7eIXI7ox1YDnp0WWJzX9">
                                <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-save"></i> Update </button>
                            </form>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>

<?php

require_once 'footer.php';
?>