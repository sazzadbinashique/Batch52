<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 4/17/2017
 * Time: 9:04 PM
 * code for interface
 *
 */

interface Lawnmower{
    public function cut_grass();
}

// does not implement
class CrappyMowerInc{
    public function leak_oil(){
        return "leaking oil";
    }
}

$mower= new CrappyMowerInc();
//$mower->leak_oil();
var_dump($mower->leak_oil());

class Kubota implements Lawnmower{
    public function cut_grass()
    {

        // TODO: Implement cut_grass() method.
        return "Cutting Major grass";
    }
}

$mower = new Kubota();
var_dump($mower->cut_grass());

class JohnDeere implements Lawnmower{
    public function cut_grass()
    {
        // TODO: Implement cut_grass() method.
        return "Cutting grass like a champion";
    }

}

$mower = new JohnDeere();
var_dump($mower->cut_grass());