<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 3/27/2017
 * Time: 8:06 PM
 */


$var = "BITM"; // data  type string
$var1 = 123456; // integer type
$var2  = true; // boolian type

$var3;
$var3 = null; // null type

$var4 = array("Sazzad", "Sumon", "Tyota");


var_dump($var);
var_dump($var1);
var_dump($var2);
var_dump($var3);

echo "<br>";

echo $var4[0];
var_dump($var4);
