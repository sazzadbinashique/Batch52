$(document).ready(function(){
    
    $(window).load(function(){
        
        var sbtn = $('.search-btn'),
        sform = $('.search-form'),
        rfclose = $('.r-close');
    
        sform.hide();
    
        sbtn.on('click' , function(){
            sform.show();
        });
    
        rfclose.on('click' , function(){
            sform.hide();
        });    
    
        
    });
    var borderH = $('.border-left, .border-right').height();
    var hh = $('.section-title').height() + 80;
    
    $('.border-left, .border-right').height( borderH - hh);
    
    $('.pskill').barIndicator();
    
    $('.testimonial-slider').owlCarousel({
        items : 2,
        autoPlay : 3000,
        pagination : true,
        navigation : true,
        navigationText : ['<i class="fa fa-reply"></i>' , '<i class="fa fa-share"></i>'],
        slideSpeed : 1100
    });
    
    $('.client-slider').owlCarousel({
        items : 1,
        autoPlay : false,
        navigation : false,
        pagination : true
    });
    
    $('.counter').counterUp({
        time : 3000
    });
    

    
    $('.project-filter').mixItUp();

});





































