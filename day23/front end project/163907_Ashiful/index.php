<?php 
$pagetitle="Home";
include('include/header.php');
include('include/projects.php');
?>

<!--Menu item-->
                        <li class="active"><a href="index.php">Home</a></li>
                        <li><a href="service.php">Services</a></li>
                        <li><a href="portfolio.php">Protfolio</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="contact.php">Contact</a></li>
                      </ul>
                    </div>
                  </div>
                </nav>
                        <!--End Nav Bar-->
  
                  <!--Slider Start-->
          <section id="slider" class="text-center wow fadeInDown">
             <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                
              </ol>

                  <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                <div class="item active">
                   <h1>Landing Page Design</h1>
                   <h2>Use HTML5 & CSS3 with Modern Tools</h2>
                      <a href="contact.php" class="btn btn-lg">Contact</a>
                </div>
                <div class="item">
                  <h1>Responcive Site Design</h1>
                   <h2>Mobile Friendly With More Features</h2>
                      <a href="contact.php" class="btn btn-lg">Contact</a>
                </div>
                  <div class="item">
                  <h1>Web Development</h1>
                   <h2>A Dyinamic Website use PHP & Mysql</h2>
                        <a href="contact.php" class="btn btn-lg">Contact</a>
                </div>
                 
              </div>

              <!-- Controls -->
              <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
            </div> 
          </section>
                        <!--Slider End-->
          
          
          <!--Last Project start -->
          
          <section id="last-project">
            <div class="container">
                  <div class="row title">
                        <div class="col-md-12 " style="font-size:20px;margin:10px;">
                              <i class="fa fa-plus-square" ></i> Last Projects
                        </div>
                  </div>
                  <div class="row">
                        <?php foreach ($projects as $project){
                              $position=$position+1;
                        if ( $total_project - $position < 3 ){
                              
                        ?> 
                        <div class="col-md-4 col-sm-4 col-xs-6">
                              <div class="project wow zoomIn" >
                                    <a href="<?php echo $project['link']?>"><img class="img-thumbnail project-img" src="<?php echo $project['img'] ?>" alt="<?php echo $project['name'] ?>"></a>
                                    <div class="img-caption">
                                          <h4><?php echo $project['name'] ?></h4>
                                          <p><?php echo $project['tag'] ?></p>
                                    </div>
                              </div>
                        </div>
                     <?php }} ?>
                  </div>
            </div>
          </section>
          
          <!--Last Project start -->
          

<!-- include footer part -->
<?php include('include/footer.php'); ?>