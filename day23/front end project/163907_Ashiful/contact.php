<?php 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $name = htmlspecialchars($_POST["name"]);
    $email = trim($_POST["email"]);
    $message = htmlspecialchars($_POST["msg"]);


    if ($name == "" OR $email == "" OR $message == "") {
        $error_msg= "You must specify a value for name, email address, and message.";
        
    }
      if (!isset($error_msg)){
          foreach( $_POST as $value ){
              if( stripos($value,'Content-Type:') !== FALSE ){
                  $error_msg= "There was a problem with the information you entered.";    

              }
          }
      }
    

    require_once("include/phpmail/class.phpmailer.php");
    $mail = new PHPMailer();
      
      if (!isset($error_msg)){
          if (!$mail->ValidateAddress($email)){
               $error_msg= "You must specify a valid email address.";

          }
      }
    if (!isset($error_msg)){
         $email_body = "";
          $email_body = $email_body . "Name: " . $name . "<br>";
          $email_body = $email_body . "Email: " . $email . "<br>";
          $email_body = $email_body . "Message: " . $message;

          $mail->SetFrom($email, $name);
          $address = "ashiful134@gmail.com";
          $mail->AddAddress($address, "BDCODEX");
          $mail->Subject    = "BDCODEX Contact Form Submission | " . $name;
          $mail->MsgHTML($email_body);

          if($mail->Send()) {
            $msg_sent="message Sent. Thanks for your message";
          }
            else {
            $error_msg="There was a problem sending the email: " . $mail->ErrorInfo;
            
            }
     }    
}
      
?>

<!--include header part-->
<?php 
$pagetitle="Contract";
include('include/header.php');

?>

<!--Menu item-->
                        <li><a href="index.php">Home</a></li>
                        <li><a href="service.php">Services</a></li>
                        <li><a href="Portfolio.php">Portfolio</a></li>
                        <li><a href="about.php">About</a></li>
                        <li class="active"><a href="contact.php">Contact</a></li>
                      </ul>
                      
                    </div>
                  </div>
                </nav>
                        <!--End Nav Bar-->
          
                       <!--Header start-->
          <section class="header wow lightSpeedIn">
          <div class="container">
                <div class="row">
                   <div class="col-md-12">
                         <div class="cnt-header">
                               <h1>Contract</h1>
                         </div>
                   </div>      
                </div>
          </div>
          </section>
                       <!--Header End-->
          
                        <!--body Start-->
          <section class="body">
            <div class="container">
                  <div class="row">
                        <div class="col-md-7 form wow zoomIn" data-wow-delay="1s">
                        <h1>Contract Form</h1>
                              <?php if(isset($error_msg)){?>
                              <div class="alert alert-danger" role="alert">
                                 <?php echo $error_msg; ?>
                              </div>
                              <?php } elseif (isset($msg_sent)) { ?>
                              <div class="alert alert-success" role="alert">
                                 <?php echo $msg_sent; ?>
                              </div>
                              <?php } ?>
                          <form action="" method="post">
                                <div class="form-group">
                                  <label for="name">Name *</label>
                                  <input type="text" class="form-control" name="name" id="name" placeholder="Jon Maker" value="<?php if (isset($name)){echo $name;}?>">
                                </div>
                                <div class="form-group">
                                  <label for="exampleInputEmail1">Email address *</label>
                                  <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Example@example.com" value="<?php if (isset($email)){echo $email;}?>">
                                </div>
                                <div class="form-group">
                                  <label for="msg">Message *</label>
                                  <textarea class="form-control" name="msg" rows="3" id="msg" value="<?php if (isset($message)){echo $message;}?>"></textarea>
                                </div>
                                <button type="submit" class="btn btn-default" name="submit">Submit</button>
                        </form>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6 hidden-xs map wow zoomIn" data-wow-delay="1.5s">
                              <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3620.4918395227405!2d89.37123346514349!3d24.847045995529953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x859c1d39a1b90215!2sSatmatha%2C+Bogra+Sadar%2C+Bogra-5800%2C+Bangladesh.!5e0!3m2!1sen!2sbd!4v1454394303064" width="370" height="220" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6 hidden-xs address wow zoomIn" data-wow-delay="1s">
                              <h3>Contract Details</h3>
                              <address>
                              Bogra,Bangladesh
                              </address>
                              <p>For Any question contract at <br>mail:ashiful134@gmail.com</p>
                        </div>
                  </div> 
            </div>
          </section>
                        <!--body End-->
          
       
          
 <!-- include footer part -->
<?php include('include/footer.php'); ?>