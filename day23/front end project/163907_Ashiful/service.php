<!--include header part-->
<?php 
$pagetitle="Services";
include('include/header.php');

?>

<!--Menu item-->
                        <li><a href="index.php">Home</a></li>
                        <li class="active"><a href="service.php">Services</a></li>
                        <li><a href="Portfolio.php">Portfolio</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="contact.php">Contact</a></li>
                      </ul>
                      
                    </div>
                  </div>
                </nav>
                        <!--End Nav Bar-->
          
                       <!--Header start-->
          <section class="header wow lightSpeedIn">
          <div class="container">
                <div class="row">
                     <div class="col-md-12">
                              <div class="service-header">
                                     <h1>Services</h1>
                               </div>
                        </div>       
                   </div>      
                </div>
          </section>
                       <!--Header End-->
          
                        <!--body Start-->
          <section class="body-service">
              <div class="container">
                <div class="row">
                      <div class="col-md-3 col-sm-3 service1">
                            <div class="service text-center wow fadeIn" data-wow-delay="0.3s">
                                  <i class="fa fa-newspaper-o" style="font-size:100px"></i>
                            <p>Landing Page Design</p>
                            </div>
                      </div> 
                      
                      <div class="col-md-3 col-sm-3 service2">
                            <div class="service text-center wow fadeIn" data-wow-delay="0.5s">
                                  <i class="fa fa-mobile" style="font-size:100px"></i>
                            <p>Responcive Site Design</p>
                            </div>
                      </div> 
                      <div class="col-md-3 col-sm-3 text-center">
                            <div class="service service3  wow fadeIn" data-wow-delay="0.7s">
                                  <i class="fa fa-unlock" style="font-size:100px"></i>
                            <p>Customize Web Site</p>
                            </div>    
                      </div>
                      <div class="col-md-3 col-sm-3 text-center">
                            <div class="service service4 wow fadeIn" data-wow-delay="0.9s">
                                  <i class="fa fa-connectdevelop" style="font-size:100px"></i>
                            <p>Web Development</p>
                            </div>    
                      </div>  
                </div>
              </div>
          </section>
          
          <section class="body2-service wow fadeIn">
                <div class="container">
                  <div class="row">
                      <div class="col-md-12 ">
                            <h1>We Are An Interactive Web Designer & Developer In Bangladesh. We Deliver World-Class WebSite
                            </h1>
                      </div>  
                </div>
              </div>
          </section>
                        <!--body End-->
          
         
         
 <!-- include footer part -->
<?php include('include/footer.php'); ?>