          <!--Blog post | Twitter Post | Subscribe Section start -->
          
          <section id="promot">
            <div class="container wow fadeIn">
                <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-6 promot-part">
                              <div class="row title">
                                    <div class="col-md-12 " style="font-size:20px;margin:10px;">
                                          <i class="fa fa-link" ></i> Last Post from Blog
                                    </div>
                              </div>
                              <div class="blog">
                                   <h4>A Field Guide To Mobile App Testing</h4>
                                    <small style="color:gray"><b>Posted:</b> Jan 05, 2015</small>
                                    <p>
                                       Testers are often thought of as people who find bugs, but have you ever considered how testers actually approach testing?<br/>
                                    </p>
                                    <a href="#">Read More <i class="fa fa-arrow-right"></i></a>      
                              </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-6 promot-part">
                        <div class="row title">
                              <div class="col-md-12 " style="font-size:20px;margin:10px;">
                                    <i class="fa fa-twitter"></i> Last Tweets
                                    </div>
                              </div>
                               <div class="twitter">
                                    <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/ashiful_islam" data-widget-id="694521305366794240">Tweets by @ashiful_islam</a>
                                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
          
                              </div>
                        </div>
                      
                        <div class="col-md-4 col-sm-4 col-xs-6 promot-part">
                             <div class="row title">
                                 <div class="col-md-12 " style="font-size:20px;margin:10px;">
                                   <i class="fa fa-send" ></i> Subscribe
                                  </div>
                              </div>
                               <div class="subscribe">
                                    <h4>Subscribe to our e-mail newsletter to receive updates.</h4>
                                     <div class="form-group input-group-btn" >
                                           <form class="input-group">
                                          <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email">
                                     <button type="button" class="btn"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>             
                                           </form>
                                           <a href="#" class="gotop" id="scrollUp">
                                                 <img src="img/top.png" alt="Go Top">
                                           </a>
                                            </div>                                                                                             
                                     </div>
                              </div>
                        </div>
                  </div>
            
          </section>
          
         <!--Blog post | Twitter Post | Subscribe Section end -->

          <!--Footer Stat-->
          <section class="footer">
            <div class="container">    
                  <div class="row text-center">
                        <div class="col-md-6 col-xs-12">
                              <p class="copy"><b>
                              Copyright <i class="fa fa-copyright"></i> <?php echo date('Y'); ?> ; All Rights Reserved.
                              </b></p>
                        </div>
                        <div class="col-md-6 col-xs-12">
                              <p class="copy-link">
                             <a href="https://www.facebook.com/ashifu134" class="btn"> <i class="fa fa-facebook"></i></a>
                              <a href="https://twitter.com/ashiful_islam" class="btn"> <i class="fa fa-twitter"></i></a>
                              <a href="https://www.linkedin.com/in/ashiful134" class="btn"><i class="fa fa-linkedin"></i></a>
                              </p>
                              
                        </div>
                  </div>
            </div>      
          </section>
           <!--Footer end-->
          
		<!--Web Contant End-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/jquery.scrollUp.js"></script>
           
            <script>
                  
            </script>

                                                 
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        
    </body>
</html>
