<!--include header part-->
<?php 
$pagetitle="Portfolio";
include('include/header.php');
include('include/projects.php');

?>

<!--Menu item-->
                         <li><a href="index.php">Home</a></li>
                        <li><a href="service.php">Services</a></li>
                        <li class="active"><a href="Portfolio.php">Portfolio</a></li>
                        <li><a href="about.php">About</a></li>
                        <li><a href="contact.php">Contact</a></li>
                      </ul>
                      
                    </div>
                  </div>
                </nav>
                        <!--End Nav Bar-->
          
                      
          
          
          <!--Last Project start -->
          
          <section id="last-project">
            <div class="container">
                  <div class="row title portfolio-title">
                        <div class="col-md-12 " style="font-size:20px;margin:10px;">
                              <i class="fa fa-th" ></i> All Projects
                        </div>
                  </div>
                  <div class="row">
                       <?php foreach ($projects as $project){?> 
                        <div class="col-md-4 col-sm-4 col-xs-6">
                              <div class="project wow zoomIn" >
                                    <a href="<?php echo $project['link']?>"><img class="img-thumbnail project-img" src="<?php echo $project['img'] ?>" alt="<?php echo $project['name'] ?>"></a>
                                    <div class="img-caption">
                                          <h4><?php echo $project['name'] ?></h4>
                                          <p><?php echo $project['tag'] ?></p>
                                    </div>
                              </div>
                        </div>
                     <?php } ?>
                   </div>
            </div>
          </section>
          
          <!--Last Project start -->
                  

 <!-- include footer part -->
<?php include('include/footer.php'); ?>