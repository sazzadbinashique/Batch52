<!--include header part-->
<?php 
$pagetitle="About Us";
include('include/header.php');

?>

<!--Menu item-->
                        <li><a href="index.php">Home</a></li>
                        <li><a href="service.php">services</a></li>
                        <li><a href="portfolio.php">Protfolio</a></li>
                        <li class="active"><a href="about.php">About</a></li>
                        <li><a href="contact.php">Contact</a></li>
                      </ul>
                      
                    </div>
                  </div>
                </nav>
                        <!--End Nav Bar-->
          
                       <!--Header start-->
          <section class="header wow lightSpeedIn">
          <div class="container">
                <div class="row">
                   <div class="col-md-12">
                         <div class="about-header">
                               <h1>About</h1>
                         </div>
                   </div>      
                </div>
          </div>
          </section>
                       <!--Header End-->
          
                        <!--body Start-->
          <section class="body">
            <div class="container">
                  <div class="row">
                        <div class="col-md-8 col-md-offset-1 about-intro wow fadeIn ">
                              <h2>We Are An Interactive Web Designer & Developer In Bangladesh. We Deliver World-Class WebSite.</h2>
                              <h3>We are a multidisciplinary team of experts with a focus on design, motion, development and                                digital strategy.</h3>
                              <p>Founded in 2009, Agency was born out of pure love for digital craftsmanship and the desire to make quality interactive work. We are a small and agile team of talented creatives and technologists that work closely with clients to deliver world-class digital experiences.

Studio specializes in concepting, art direction, design and technical realization. Our work spans across all mediums and platforms, partnering with both brands and agencies around the globe. Our team has over 10 years of experience in the interactive field, with expertise in Information Architecture, User Experience Design and Creative Programming
                              </p><br>
                              
                              <h3>How We Work</h3>
                              <h4>Phase 1: Discovery</h4>

                             <p> During the initial stage of a project, the client provides a clearly developed brief. Through both creative concepting and technical discovery, the brief is thoroughly addressed. The discovery phase takes place along-side the client to set a clear foundation for the project.</p>

                              <h4>Phase 2: Pre-production / Creation</h4>

                              <p>After the discovery phase, a final creative and technical direction is developed. Prototypes are created while mechanics and behaviors are refined. The concept and creative execution are exhaustively explored.</p>

                              <h4>Phase 3: Production</h4>
                              <p>In the final phase, all pages defined in the Interactive Design Document (IDD) are executed. This includes tasks such as programming, animation, sound design and asset production. Assets and behaviors are continuously polished until the site enters a testing phase.</p>
                        </div>
                     <div class="col-md-3 about-me text-center">
                           <i class="fa fa-user" style="font-size:24px; color:white ">  Personal Details</i>
                      </div>
                        <div class="col-md-3 text-center about-me wow zoomIn">
                           <img src="img/ashif.jpg" alt="owner" class="img-responsive thumbnail img-rounded" style="">
                              <br>
                              <h3>A.M.Ashiful Islam</h3>
                              <small>Web Designer & Developer</small>
                              <address>
                              Bogra,Bangldesh
                              </address>
                              <table>
                                    <tr>
                                          <td><i class="fa fa-phone"></i> Phone:</td><td>+8801723505527</td>
                                    </tr>
                                    <tr>
                                          <td><i class="fa fa-google"></i> GMail: </td>
                                          <td>ashiful134@gmail.com</td>
                                    </tr>
                                    <tr>
                                          <td><i class="fa fa-yahoo"></i> Yahoo: </td>
                                          <td>ashiful134@yahoo.com</td>
                                    </tr>
                                    <tr>
                                          <td><i class="fa fa-facebook"></i> Facebook: </td>
                                          <td>fb.com/ashiful134</td>
                                    </tr>
                                    <tr>
                                          <td><i class="fa fa-twitter"></i>  Twitter: </td>
                                          <td>twitter.com/ashiful_islam</td>
                                    </tr>
                              
                              </table>
                      </div>
                  </div>          
            </div>
          </section>
                        <!--body End-->
          
            

          
 <!-- include footer part -->
<?php include('include/footer.php'); ?>