$(document).ready(function(){
   $('.counter').counterUp();
    
    //isotope
    var work = $('.works').isotope({
                //option
                itemSelector:'.single-work',
                layoutMode:'masonry',
                masonry:{
                    columnWidth:1  
                },
                filter:'*'
            });
    $('.work-cat li').on('click', function(){
        $('.work-cat li').removeClass('active');
		$(this).addClass('active');
        var selector = $(this).data('filter');
        
//        console.log(selector)
        work.isotope({
           filter:selector 
        });
    });
    
    var eimg = $('.event-img');
    eimg.each(function(){
        var eheight = $(this).height();
        $(this).siblings().css('height', eheight+'px');
    });
    
    $(".testimonial-slider").owlCarousel({
        items:2,
        margin:30
        
    });
    
});