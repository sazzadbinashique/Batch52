<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop - PH
 * Date: 4/10/2017
 * Time: 5:58 PM
 */

class User {

    public $firstName;
    public $lastName;

    public function hello()
    {
        return "hello";
    }
}


$user1 = new User();
$user1->firstName = 'John';
$user1->lastName  = 'Doe';

//echo $user1->firstName. " " .$user1->lastName;

$hello = $user1->hello();

//echo $hello . ",  " . $user1->firstName . " " . $user1->lastName;

$user2 = new User;
$user2 ->firstName = 'Jane';
$user2 ->lastName  = 'Doe';


echo $hello . ",  " . $user1->firstName . " " . $user1->lastName;

echo $hello . ",  " . $user2->firstName . " " . $user2->lastName;