<?php

namespace BITM\Sazzad;

class BWW
{
 public $color_name = '';
 public $color_code = '';


public function __construct($cname, $ccode)
{

    $this->color_name= $cname;
    $this->color_code= $ccode;

}

public function  __toString()
{

    return $this->color_name. " ".$this->color_code;
}

}